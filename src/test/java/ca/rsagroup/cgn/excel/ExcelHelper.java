package ca.rsagroup.cgn.excel;

import java.io.*;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import ca.rsagroup.cgn.dto.TimesheetDetailsDTO;
import ca.rsagroup.cgn.model.Timesheet;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;  
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath*:**/persistence-config.xml"})
public class ExcelHelper extends AbstractJUnit4SpringContextTests {

    @Autowired
    private ApplicationContext context;

	private HSSFWorkbook wb;
	
	private HSSFSheet sheet;
	
	public ExcelHelper(){
		wb = new HSSFWorkbook();
		sheet = wb.createSheet("TimeReport_");
	}


	
	public void create( Map<Long, EmpTimesheetDetailsRowMap> detailsMap, List<Date> daysInTheTimeSheetPeriod){
		
		Row header = sheet.createRow(1);
		header.setHeightInPoints(45);
        HSSFCellStyle myStyle = wb.createCellStyle();
        myStyle.setRotation((short)90);
        int position = 0;
        createCellHeader(header, (short)position++, CellStyle.ALIGN_CENTER, CellStyle.VERTICAL_BOTTOM, "Resource Name",4000,(short)10);
        createCellHeader(header, (short)position++, CellStyle.ALIGN_CENTER, CellStyle.VERTICAL_BOTTOM, "Role",7500,(short)10);
        createCellHeader(header, (short)position++, CellStyle.ALIGN_CENTER, CellStyle.VERTICAL_BOTTOM, "Location",4000,(short)10);
        createCellHeader(header, (short)position++, CellStyle.ALIGN_CENTER, CellStyle.VERTICAL_BOTTOM, "Rate",4000,(short)10);
        createCellHeader(header, (short)position++, CellStyle.ALIGN_CENTER, CellStyle.VERTICAL_BOTTOM, "Project/Activity",4000,(short)10);
        createCellHeader(header, (short)position++, CellStyle.ALIGN_CENTER, CellStyle.VERTICAL_BOTTOM, "Project ID",4000,(short)10);
        
        for(Date timeHeader: daysInTheTimeSheetPeriod) {
           Cell cell= createCellHeader(header, (short) position++, CellStyle.ALIGN_CENTER, CellStyle.VERTICAL_BOTTOM, getFormattedDateForHeader(timeHeader), 1000, (short) 10);
            CellStyle cellStyle = cell.getCellStyle();
            cellStyle.setRotation((short) 90);
            cellStyle.setFont(createUnBoldFont((short) 9));
            cellStyle.setFillBackgroundColor(HSSFColor.GREY_25_PERCENT.index);
            cellStyle.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
            cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            cell.setCellStyle( cellStyle );

        }
        createCellHeader(header, (short) position++, CellStyle.ALIGN_CENTER, CellStyle.VERTICAL_BOTTOM, "Total", 7500, (short) 10);
        int rownum = 2;
        for(Long employeeId: detailsMap.keySet() ) {
            EmpTimesheetDetailsRowMap rowMap = detailsMap.get(employeeId);

            for (Long taskId: rowMap.getTimeSheetDetailRowMap().keySet()) {
                TimeSheetDetailRow timesheetDetail = rowMap.getTimeSheetDetailRowMap().get( taskId );
                Row row = sheet.createRow(rownum++);
                int cellnum = 0;
                populateCell(row, cellnum++, timesheetDetail.getEmployeeName(),HSSFColor.LIGHT_GREEN.index);
                populateCell(row, cellnum++, timesheetDetail.getEmployeeRole(),HSSFColor.LIGHT_GREEN.index);
                populateCell(row, cellnum++, timesheetDetail.getLocation(),HSSFColor.LIGHT_GREEN.index);
                populateCell(row, cellnum++, timesheetDetail.getRate(),HSSFColor.LIGHT_GREEN.index);
                populateCell(row, cellnum++, timesheetDetail.getTaskName(),HSSFColor.LIGHT_GREEN.index);
                populateCell(row, cellnum++, timesheetDetail.getProjectId(),HSSFColor.LIGHT_GREEN.index);

                for(Date day: daysInTheTimeSheetPeriod){
                    Short index = HSSFColor.LIGHT_GREEN.index;
                    if(isWeekend(day)){
                        index = HSSFColor.LIGHT_BLUE.index;
                    }
                    if( timesheetDetail.getHours(getFormattedDate(day))!=null){

                        Cell cell = populateCell(row, cellnum++, timesheetDetail.getHours(getFormattedDate(day)),index);
                    }else{
                        Cell cell = populateCell(row, cellnum++, null,index);
                    }
                }
            }
            insertBlankRow(rownum,daysInTheTimeSheetPeriod);
            rownum++;
        }
        
        sheet.autoSizeColumn((short)2);

        try {
        	File file = new File("C:\\Users\\t900435\\Desktop\\new.xls");
        	if(file.exists()) file.delete();
            FileOutputStream out =
                    new FileOutputStream(new File("C:\\Users\\t900435\\Desktop\\new.xls"));
            wb.write( out );

            out.close();


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


		
	}

    public String getFormattedDate( Date date ){
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        return df.format(date);
    }

    @Test
    public void generateExcel(){
        SessionFactory factory = (SessionFactory)context.getBean("mySessionFactory");
        Session session = factory.openSession();
//        List list1 = session.getNamedQuery("getTimeSheetDetails").list();
        Query query = session.getNamedQuery("getTimeSheetDetails");

       query.setResultTransformer(Transformers.aliasToBean(TimesheetDetailsDTO.class));

        List<TimesheetDetailsDTO> list = query.list();

       Timesheet tm = (Timesheet) session.createQuery("From Timesheet tm where tm.isActive=true ").uniqueResult();

        GenerateExcelHelper helper = new GenerateExcelHelper();
        create(buildEmployeeTimesheetDetails(list), getDaysBetweenDates(tm.getStartDate(), tm.getEndDate()));
//        return list;
    }

    private Map<Long, EmpTimesheetDetailsRowMap> buildEmployeeTimesheetDetails(List<TimesheetDetailsDTO> timesheetDetailsDTOList){

        Map<Long, EmpTimesheetDetailsRowMap> empTimesheetDetailsRowMapMap = new HashMap<Long,EmpTimesheetDetailsRowMap>();

        for(TimesheetDetailsDTO timesheetDetailsDTO: timesheetDetailsDTOList){
            EmpTimesheetDetailsRowMap empTimesheetDetailsRowMap = empTimesheetDetailsRowMapMap.get(timesheetDetailsDTO.getEmployeeId());
            if(empTimesheetDetailsRowMap==null){
                empTimesheetDetailsRowMap = new EmpTimesheetDetailsRowMap();
                empTimesheetDetailsRowMap.setEmployeeId( timesheetDetailsDTO.getEmployeeId());
                empTimesheetDetailsRowMapMap.put( timesheetDetailsDTO.getEmployeeId(),empTimesheetDetailsRowMap);
            }
            TimeSheetDetailRow row = empTimesheetDetailsRowMap.getTimeSheetDetailRow(timesheetDetailsDTO.getTaskId(), timesheetDetailsDTO);
            row.getHoursMap().put(getFormattedDate(timesheetDetailsDTO.getWorkDate()),timesheetDetailsDTO.getHours());
        }

        return empTimesheetDetailsRowMapMap;

    }

     private void insertBlankRow(int rowNum, List<Date> daysInTheTimeSheetPeriod){
         Row row = sheet.createRow(rowNum++);
         int cellnum = 0;
         populateCell(row, cellnum++, null,HSSFColor.LIGHT_GREEN.index);
         populateCell(row, cellnum++, null,HSSFColor.LIGHT_GREEN.index);
         populateCell(row, cellnum++, null,HSSFColor.LIGHT_GREEN.index);
         populateCell(row, cellnum++, null,HSSFColor.LIGHT_GREEN.index);
         populateCell(row, cellnum++, null,HSSFColor.LIGHT_GREEN.index);
         populateCell(row, cellnum++, null,HSSFColor.LIGHT_GREEN.index);

         for(Date day: daysInTheTimeSheetPeriod){
             Short index = HSSFColor.LIGHT_GREEN.index;
             if(isWeekend(day)){
                 index = HSSFColor.LIGHT_BLUE.index;
             }
                 Cell cell = populateCell(row, cellnum++, null,index);
         }

     }

    public List<Date> getDaysBetweenDates(Date startdate, Date enddate)
    {
        List<Date> dates = new ArrayList<Date>();
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(startdate);

        while (calendar.getTime().compareTo(enddate)<=0)
        {
            Date result = calendar.getTime();
            dates.add(result);
            calendar.add(Calendar.DATE, 1);
        }
        return dates;
    }

    private Date getDate(int day, int month, int year){
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_MONTH, day);
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.YEAR, year);
        return cal.getTime();
    }

    private String getFormattedDateForHeader(Date date){
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        return cal.get(Calendar.DAY_OF_MONTH)+"-"+cal.getDisplayName(Calendar.MONTH, Calendar.SHORT,Locale.ENGLISH);
    }

    public boolean isWeekend(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime( date );
        int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
        return (Calendar.SUNDAY == dayOfWeek || Calendar.SATURDAY == dayOfWeek);
    }

	private void populateRows(
            EmpTimesheetDetailsRowMap map, List<Date> daysInTimesheetPeriod ) {

        int rownum = 2;
        for (Long taskId: map.getTimeSheetDetailRowMap().keySet()) {
            TimeSheetDetailRow timesheetDetail = map.getTimeSheetDetailRowMap().get( taskId );
             Row row = sheet.createRow(rownum++);
            int cellnum = 0;
            populateCell(row, cellnum++, timesheetDetail.getEmployeeName(),HSSFColor.LIGHT_GREEN.index);
            populateCell(row, cellnum++, timesheetDetail.getEmployeeRole(),HSSFColor.LIGHT_GREEN.index);
            populateCell(row, cellnum++, timesheetDetail.getLocation(),HSSFColor.LIGHT_GREEN.index);
            populateCell(row, cellnum++, timesheetDetail.getRate(),HSSFColor.LIGHT_GREEN.index);
            populateCell(row, cellnum++, timesheetDetail.getTaskName(),HSSFColor.LIGHT_GREEN.index);
            populateCell(row, cellnum++, timesheetDetail.getProjectId(),HSSFColor.LIGHT_GREEN.index);

            for(Date day: daysInTimesheetPeriod){
                Short index = HSSFColor.LIGHT_GREEN.index;
                if(isWeekend(day)){
                    index = HSSFColor.LIGHT_BLUE.index;
                }
                if( timesheetDetail.getHours(getFormattedDate(day))!=null){

                    Cell cell = populateCell(row, cellnum++, timesheetDetail.getHours(getFormattedDate(day)),index);
                }else{
                    Cell cell = populateCell(row, cellnum++, null,index);
                }
            }
        }
	}

    private void changeBackgroundColorIfDayIsWeekend(Cell cell, Date workDate) {

        if(isWeekend(workDate)){
            CellStyle cellStyle = cell.getCellStyle();
            cellStyle.setFillBackgroundColor(HSSFColor.LIGHT_BLUE.index);
            cellStyle.setFillForegroundColor(HSSFColor.LIGHT_BLUE.index);
            cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            cell.setCellStyle( cellStyle );
        }

    }


    private Cell populateCell(Row row, int cellnum, Object obj, Short colorIndex) {
		Cell cell = row.createCell(cellnum++);
		if(obj instanceof Date)
		    cell.setCellValue((Date)obj);
		else if(obj instanceof Boolean)
		    cell.setCellValue((Boolean)obj);
		else if(obj instanceof String)
		    cell.setCellValue((String)obj);
		else if(obj instanceof Double)
		    cell.setCellValue((Double)obj);
        else if(obj instanceof  Integer)
           cell.setCellValue( String.valueOf(obj) );
        else if(obj instanceof BigDecimal)
            cell.setCellValue( ((BigDecimal)obj).doubleValue() );
		CellStyle cellStyle = wb.createCellStyle();
        cellStyle.setAlignment(CellStyle.ALIGN_CENTER);
        cell.setCellStyle( cellStyle );
        cellStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        cellStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
        cellStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        cellStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);

        cellStyle.setFont(createUnBoldFont((short) 9));
        cellStyle.setFillBackgroundColor(colorIndex);
        cellStyle.setFillForegroundColor(colorIndex);
        cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        return cell;
	}
	
	private Cell createCellHeader(Row row, short column, short halign, short valign, String value, int width, short fontHeights ) {
        Cell cell = row.createCell(column);
        cell.setCellValue(value);
        sheet.setColumnWidth(column, width);
        CellStyle cellStyle = wb.createCellStyle(); 
        cellStyle.setAlignment(halign);
//        cellStyle.setFillBackgroundColor(HSSFColor.LIGHT_CORNFLOWER_BLUE.index);
//        cellStyle.setFillForegroundColor(HSSFColor.LIGHT_CORNFLOWER_BLUE.index);
//        cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        cellStyle.setVerticalAlignment(valign);
        cellStyle.setFont( createFont(fontHeights) );
        cellStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        cellStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
        cellStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        cellStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        cell.setCellStyle(cellStyle);
        return cell;
	}
	 
	 private Font createFont(Short fontHeights){
        Font font = wb.createFont();
        font.setBoldweight(Font.BOLDWEIGHT_BOLD);
        font.setFontHeightInPoints((short)fontHeights);
        return font;
    }

    private Font createUnBoldFont(Short fontHeights){
        Font font = wb.createFont();
        font.setBoldweight(Font.BOLDWEIGHT_NORMAL);
        font.setFontHeightInPoints((short)fontHeights);
        return font;
    }
	 
}
