package ca.rsagroup.cgn.excel;

import ca.rsagroup.cgn.model.Employee;
import ca.rsagroup.cgn.model.Task;
import ca.rsagroup.cgn.model.TimesheetDetail;
import org.jsefa.Deserializer;
import org.jsefa.common.lowlevel.filter.HeaderAndFooterFilter;
import org.jsefa.csv.CsvIOFactory;
import org.jsefa.csv.config.CsvConfiguration;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by t900435 on 3/19/2015.
 */
public class ParseCSV {

   // @Test
    public void parse() throws FileNotFoundException {
        File file = new File("C:\\Test.csv");

        FileReader reader = new FileReader( file );
        CsvConfiguration config = new CsvConfiguration();
        // header of size 1, no footer, store the filtered lines
//        config.setLineFilter(new HeaderAndFooterFilter(1, false, true));
        config.setFieldDelimiter(',');
        Deserializer deserializer = CsvIOFactory.createFactory(config,CSVToPOJO.class).createDeserializer();
        deserializer.open( reader);
        CSVToPOJO headerObj = null;
        List<CSVToPOJO> dataObjectsList = new ArrayList<CSVToPOJO>();
        List<TimesheetDetail> timesheetDetailsList = new ArrayList<TimesheetDetail>();
        while(deserializer.hasNext()){
            if(headerObj==null) headerObj = deserializer.next();
            else{
                CSVToPOJO pojo = deserializer.next();
                dataObjectsList.add(pojo);
                Task task = new Task();
                task.setTaskName(pojo.getProjectActivity());
            }
            
        }


        for(CSVToPOJO pojo: dataObjectsList){
            parseTimesheetdetails(pojo, headerObj, timesheetDetailsList);
        }

    }

    @Test
    public void parseTaskName() throws FileNotFoundException {
        File file = new File("C:\\Test.csv");

        FileReader reader = new FileReader( file );

        CsvConfiguration config = new CsvConfiguration();
        // header of size 1, no footer, store the filtered lines
        config.setLineFilter(new HeaderAndFooterFilter(1, false, true));
        config.setFieldDelimiter(',');
        Deserializer deserializer = CsvIOFactory.createFactory(config,CSVToTaskPojo.class).createDeserializer();
        deserializer.open( reader);
        while(deserializer.hasNext()){
            CSVToTaskPojo pojo = deserializer.next();
            System.out.println( pojo.getTaskName());

        }


    }
    
    private void  parseTimesheetdetails(CSVToPOJO pojo, CSVToPOJO headerObj, List<TimesheetDetail> detailList){
        if( pojo.getDate1() !=null && pojo.getDate1().trim()!=null  ){
            detailList.add(mapTimeSheetDetail(pojo,headerObj.getDate1(),pojo.getDate1()));
        }
        if( pojo.getDate1() !=null && pojo.getDate2().trim()!=null  ){
            detailList.add(mapTimeSheetDetail(pojo,headerObj.getDate2(),pojo.getDate2()));
        }
        if( pojo.getDate2() !=null && pojo.getDate3().trim()!=null  ){
            detailList.add(mapTimeSheetDetail(pojo,headerObj.getDate3(),pojo.getDate3()));
        }
        if( pojo.getDate3() !=null && pojo.getDate4().trim()!=null  ){
            detailList.add(mapTimeSheetDetail(pojo,headerObj.getDate4(),pojo.getDate4()));
        }
        if( pojo.getDate4() !=null && pojo.getDate5().trim()!=null  ){
            detailList.add(mapTimeSheetDetail(pojo,headerObj.getDate5(),pojo.getDate5()));
        }
        if( pojo.getDate5() !=null && pojo.getDate6().trim()!=null  ){
            detailList.add(mapTimeSheetDetail(pojo,headerObj.getDate6(),pojo.getDate6()));
        }
        if( pojo.getDate7() !=null && pojo.getDate7().trim()!=null  ){
            detailList.add(mapTimeSheetDetail(pojo,headerObj.getDate7(),pojo.getDate7()));
        }
    }

    private TimesheetDetail mapTimeSheetDetail(CSVToPOJO pojo, String date, String hours){
        TimesheetDetail detail = new TimesheetDetail();
        detail.setTask( mapTaskName( pojo) );
        detail.setEmployee( mapEmployeeDetails(pojo));
        detail.setWorkDate( getDate(date) );
        detail.setHours( BigDecimal.valueOf(Long.valueOf(hours)) );
        return detail;
    }

    private Employee mapEmployeeDetails(CSVToPOJO pojo){
        Employee employee = new Employee();
        String[] name = pojo.getResourceName().split(" ");
        employee.setFirstname( name[0] );
        employee.setLastname( name[1] );
        return employee;
    }

    private Task mapTaskName(CSVToPOJO pojo){
        Task task = new Task();
        task.setTaskName( pojo.getProjectActivity() );
        return task;
    }

    private Date getDate(String date){
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-YYYY");
        try {
            return format.parse( date );
        } catch (ParseException e) {
            // e.printStackTrace();
            return null;
        }
    }
}
