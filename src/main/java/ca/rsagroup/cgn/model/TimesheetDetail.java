package ca.rsagroup.cgn.model;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by t900435 on 3/18/2015.
 */
@Entity
@Table(name="timesheet_details")
public class TimesheetDetail implements Serializable {


    private static final long serialVersionUID = -2710635171917810262L;

    @Id
    @GeneratedValue
    @Column(name="timesheet_details_id")
    private Long id;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="employee_id", insertable=true, updatable = true)
    private Employee employee;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="timesheet_id", insertable=true, updatable = true)
    private Timesheet timesheet;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="task_id", insertable=true, updatable = true)
    private Task task;

    @Temporal(TemporalType.DATE)
    @Column(name="work_day")
    private Date workDate;

    @Column(name="hours")
    private BigDecimal hours;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Timesheet getTimesheet() {
        return timesheet;
    }

    public void setTimesheet(Timesheet timesheet) {
        this.timesheet = timesheet;
    }

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }

    public Date getWorkDate() {
        return workDate;
    }

    public void setWorkDate(Date workDate) {
        this.workDate = workDate;
    }

    public BigDecimal getHours() {
        return hours;
    }

    public void setHours(BigDecimal hours) {
        this.hours = hours;
    }
}
