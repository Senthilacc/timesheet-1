package ca.rsagroup.cgn.model;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by t900435 on 3/18/2015.
 */
@Entity
@Table(name="timesheet")
public class Timesheet implements Serializable {

    private static final long serialVersionUID = -2173747608214907669L;

    @Id
    @GeneratedValue
    private Long id;

    @Temporal(TemporalType.DATE)
    @Column(name="start_date")
    @NotNull
    private Date startDate;

    @Temporal(TemporalType.DATE)
    @Column(name="end_date")
    @NotNull
    private Date endDate;

    @Type(type = "yes_no")
    @Column(name="status" )
    private Boolean isActive;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
}
