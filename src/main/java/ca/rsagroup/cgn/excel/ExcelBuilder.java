package ca.rsagroup.cgn.excel;

import ca.rsagroup.cgn.dto.TimesheetDetailsDTO;
import ca.rsagroup.cgn.model.Timesheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.document.AbstractExcelView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * Created by t900435 on 3/22/2015.
 */
@Component
public class ExcelBuilder  extends AbstractExcelView{

    @Override
    protected void buildExcelDocument(Map<String, Object> map, HSSFWorkbook hssfWorkbook, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
        List<TimesheetDetailsDTO> list = (List<TimesheetDetailsDTO>) map.get("list");
        Timesheet tm = (Timesheet) map.get("tm");
        new GenerateExcelHelper().generateExcel(hssfWorkbook, list, tm);
    }
}
