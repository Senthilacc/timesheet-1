package ca.rsagroup.cgn.dao.impl;

import ca.rsagroup.cgn.dao.TimeSheetDAO;
import ca.rsagroup.cgn.model.Employee;
import ca.rsagroup.cgn.dto.SubmittedEntriesDTO;
import ca.rsagroup.cgn.dto.TimesheetDetailsDTO;
import ca.rsagroup.cgn.model.Task;
import ca.rsagroup.cgn.model.Timesheet;
import ca.rsagroup.cgn.model.TimesheetDetail;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by t900435 on 3/19/2015.
 */
@Repository
public class TimeSheetDAOImpl implements TimeSheetDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Timesheet getActiveTimesheet() {
      return (Timesheet) getCurrentSession().createQuery("From Timesheet tm where tm.isActive=true ").uniqueResult();
    }

    @Override
    public Employee saveOrLoadEmployee(Employee employee) {
        List<Employee> employees = getCurrentSession().
                createQuery("from Employee emp where emp.firstname = :firstName and emp.lastname = :lastName ").
                setString("firstName", employee.getFirstname()).setString("lastName", employee.getLastname()).list();
        if(employees.isEmpty()){
            employee.setProjectRole("Senior Software Developer");
            getCurrentSession().save(employee);
            return employee;
        }
        return employees.get(0);
    }

    @Override
    public Task saveOrLoadTask(Task task) {

        Session currentSession = getCurrentSession();

        List<Task> taskList = currentSession.createQuery("from Task task where lower(task.taskName) = :TaskName ").
                setString("TaskName", task.getTaskName().toLowerCase()).list();
        if(taskList.isEmpty()){
            currentSession.save( task );
            return task;
        }
        return taskList.get(0);
    }

    @Override
    public void saveOrUpdate(TimesheetDetail timesheetDetail) {

        timesheetDetail.setEmployee( saveOrLoadEmployee(timesheetDetail.getEmployee()) );
        timesheetDetail.setTask( saveOrLoadTask(timesheetDetail.getTask()) );
        timesheetDetail.setTimesheet( getActiveTimesheet());
        getCurrentSession().saveOrUpdate(timesheetDetail);

    }
    @Override
    public void deleteTimesheet(Employee employee, Long timesheetId){
        Criteria criteria = getCurrentSession().createCriteria(TimesheetDetail.class);
        criteria.createAlias("employee", "emp");
        criteria.createAlias("timesheet", "tm");
        criteria.add(Restrictions.eq("emp.firstname",employee.getFirstname()));
        criteria.add(Restrictions.eq("emp.lastname", employee.getLastname()));
        criteria.add(Restrictions.eq("tm.id",timesheetId));
        List<TimesheetDetail> list = criteria.list();
        for(TimesheetDetail tm: list){
            getCurrentSession().delete(tm);
        }
    }

    @Override
    public List<SubmittedEntriesDTO> getSubmittedEntries() {
        Query query = getCurrentSession().getNamedQuery("submittedEntries");
        query.setResultTransformer(Transformers.aliasToBean(SubmittedEntriesDTO.class));
        List<SubmittedEntriesDTO> list1 = query.list();
       return list1;
    }

    @Override
    public List<TimesheetDetailsDTO> getTimeSheetDetailEntries() {

       Query query =  getCurrentSession().getNamedQuery("getTimeSheetDetails");

        query.setResultTransformer(Transformers.aliasToBean(TimesheetDetailsDTO.class));

        List<TimesheetDetailsDTO> list = query.list();
        return list;
    }

    private Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }
}
