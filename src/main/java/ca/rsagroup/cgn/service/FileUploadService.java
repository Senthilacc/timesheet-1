package ca.rsagroup.cgn.service;

import ca.rsagroup.cgn.dto.SubmittedEntriesDTO;

import java.io.Reader;
import java.util.List;

/**
 * Created by t900435 on 3/21/2015.
 */
public interface FileUploadService {
    /**
     * This method uploads timesheet for a single employee in CSV format.
     */
    public void uploadTimeSheet(Reader reader);

    public List<SubmittedEntriesDTO> getSubmittedEntries();
}
