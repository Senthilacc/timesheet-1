package ca.rsagroup.cgn.service.impl;

import ca.rsagroup.cgn.dao.TimeSheetDAO;
import ca.rsagroup.cgn.dto.SubmittedEntriesDTO;
import ca.rsagroup.cgn.helper.ParseCSV;
import ca.rsagroup.cgn.model.TimesheetDetail;
import ca.rsagroup.cgn.service.FileUploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.Reader;
import java.util.List;

/**
 * Created by t900435 on 3/21/2015.
 */
@Service("fileUploadService")
public class FileUploadServiceImpl implements FileUploadService{

    @Autowired
    private TimeSheetDAO timeSheetDAO;

    @Resource(name="parseCSV")
    private ParseCSV parseCSV;

    @Override
    @Transactional
    public void uploadTimeSheet(Reader reader) {
       List<TimesheetDetail> list= parseCSV.parseCSV(reader);
        if(list!=null && !list.isEmpty()) {
            TimesheetDetail dtl = list.get(0);
            timeSheetDAO.deleteTimesheet( dtl.getEmployee(), timeSheetDAO.getActiveTimesheet().getId() );
            for (TimesheetDetail timesheetDetail : list) {
                timeSheetDAO.saveOrUpdate(timesheetDetail);
            }
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<SubmittedEntriesDTO> getSubmittedEntries() {
        return timeSheetDAO.getSubmittedEntries();
    }
}
